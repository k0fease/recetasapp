package com.example.recetasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText etIngresar;
    private Button btBuscar;
    ResultadoActivity resultado;
    private TextView tvIngredientes;
    private TextView tvInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etIngresar = findViewById(R.id.etIngresar);
        this.btBuscar = findViewById(R.id.btBuscar);
        this.tvIngredientes = (TextView) findViewById(R.id.tvIngredientes);
        this.tvInfo = (TextView) findViewById(R.id.tvInfo);

        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String busqueda = etIngresar.getText().toString().trim();

                if (!busqueda.equals("")) {

                    String url = "https://api.edamam.com/search?q=" + busqueda + "&app_id=a5e50568&app_key=30927dcfe4bdc4f1f2a68b37466fae97&from=0&to=1";
                    StringRequest solicitud = new StringRequest(
                            Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //Toast.makeText(getApplicationContext(), "sii hay respuesta", Toast.LENGTH_SHORT).show();
                                    try {

                                        JSONObject respuestaJSON = new JSONObject(response);
                                        JSONArray hits = respuestaJSON.getJSONArray("hits");
                                        JSONObject cero = hits.getJSONObject(0);
                                        JSONObject recipe = cero.getJSONObject("recipe");
                                        JSONArray ingredientL = recipe.getJSONArray("ingredientLines");
                                        String ingrediente = "";

                                        for(int f=0; f<ingredientL.length(); f++)
                                        {
                                            ingrediente = ingrediente + ingredientL.getString(f)+ "\n";
                                        }

                                        //tvIngredientes.setText(ingrediente);

                                        JSONObject  totalN = recipe.getJSONObject("totalNutrients");

                                        JSONObject kcal = totalN.getJSONObject("ENERC_KCAL");
                                        String label = kcal.getString("label");
                                        String quantity = kcal.getString("quantity");
                                        String unit = kcal.getString("unit");

                                        JSONObject fat = totalN.getJSONObject("FAT");
                                        String label2 = fat.getString("label");
                                        String quantity2 = fat.getString("quantity");
                                        String unit2 = fat.getString("unit");

                                        JSONObject fasat = totalN.getJSONObject("FASAT");
                                        String label3 = fasat.getString("label");
                                        String quantity3 = fasat.getString("quantity");
                                        String unit3 = fasat.getString("unit");

                                        String nombre = recipe.getString("label");

                                        tvIngredientes.setText(nombre+"\n"+"\n"+"INGREDIENTES"+"\n"+"\n"+ingrediente+"\n"+"\n"+"INFORMACIÓN NUTRICIONAL"+"\n"+"\n"+label+" ... "+quantity+" ... "+unit+ "\n" +
                                                label2+" ... "+quantity2+" ... "+unit2+ "\n" +
                                                label3+" ... "+quantity3+" ... "+unit3);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                       // Toast.makeText(getApplicationContext(), "No hay respuesta", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "no hay respuesta" + error, Toast.LENGTH_SHORT).show();
                                }
                            }

                    );

                    RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                    listaEspera.add(solicitud);

                } else {
                    Toast.makeText(getApplicationContext(), "Ingrese texto de búsqueda", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvIngredientes.setMovementMethod(new ScrollingMovementMethod());
    }
}


