package com.example.recetasapp;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultadoActivity extends AppCompatActivity {

    private TextView tvIngredientes;
    private TextView tvInfo;
    public String variable;

   // public void setVariable(String variable) {
     //   this.variable = variable;
    //}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        this.tvIngredientes = (TextView) findViewById(R.id.tvIngredientes);
        this.tvInfo = (TextView) findViewById(R.id.tvInfo);

        String url = "https://api.edamam.com/search?q=pollo&app_id=a5e50568&app_key=30927dcfe4bdc4f1f2a68b37466fae97&from=0&to=1";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        Toast.makeText(getApplicationContext(), "sii hay respuesta", Toast.LENGTH_SHORT).show();
                    try {

                        JSONObject respuestaJSON = new JSONObject(response);
                        JSONArray hits = respuestaJSON.getJSONArray("hits");
                        JSONArray cero = hits.getJSONArray(0);
                        JSONArray recipe = cero.getJSONArray(10);


                        tvIngredientes.setText("Ingredientes: "+recipe);

                    } catch (JSONException e){
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "No hay respuesta", Toast.LENGTH_SHORT).show();
                    }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "noo hay respuesta"+error, Toast.LENGTH_SHORT).show();
                    }
                }

        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);

    }
}
